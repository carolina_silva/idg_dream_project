# IDG-DREAM Drug-Kinase Binding Prediction Challenge #

## Introduction ##

Kinases are protein enzymes responsible for the reversible phosphorylation in proteins. This mechanism is responsible for function regulation in many proteins of the proteome. After the completion of the human genome nearly all of the human kinome was identified. The total amount of kinases identified through this process was 518.  

Protein kinases work by changing substrate activity. This modification regulates many processes in the cell, like metabolism, transcription, cell cycle progression, cytoskeletal rearrangement, cell movement, apoptosis and differentiation. Because of such important functions on the organism homeostasis, mutations on the genes encoding kinases usually lead to Human disease. Therefore, agonists and antagonists of these proteins are possible and important targets for the development of many new therapeutics.

The IDG-DREAM Drug-Kinase Binding Prediction Challenge main purpose is to use machine learning, statistical and other computational tools to predict the interaction between kinase inhibitors, an important drug class, and their targets. Such approaches in drug discovery and effectiveness are important since they reduce the amount of large expenses related to drug discovery and reduce the time needed in this process.


## Raw Data Selection ##

Collecting the right information is the most crucial step in machine learning. In this case, KD values of drug-kinase interactions have to be precise and accurate so their prediction is also trustworthy.

The wiki of the challenge suggests using the data of the website DrugTargetCommons, a crowd-sourcing platform to improve the consensus and use of drug-target interactions, as training data. As such, the dataset was downloaded using the UNIX command wget:

```bash
wget https://drugtargetcommons.fimm.fi/static/Excell_files/DTC_data.csv  
```

Only the rows containing any form of KD in the column standard_type of the dataset downloaded were selected, due to the fact that the column standard_type contains several measures that can not be converted to KD values. This was achieved by using the following UNIX commands:

```bash
egrep ',[^,]*[Kk]{1}[Dd]{1}[^,"]*,(=|>){1},' ../../../data/Preprocessed/DrugTargetCommons/DTC_data.csv > ../../../data/Processed/Model_Creation/1-After_KD_Selection/KD_dataset.csv 
sed -i '1i compound_id,standard_inchi_key,compound_name,synonym,target_id,target_pref_name,gene_names,wildtype_or_mutant,mutation_info,pubmed_id,standard_type,standard_relation,standard_value,standard_units,ep_action_mode,assay_format,assaytype,assay_subtype,inhibitor_type,detection_tech,assay_cell_line,compound_concentration_value,compound_concentration_value_unit,substrate_type,substrate_relation,substrate_value,substrate_units,assay_description,title,journal,doc_type,annotation_comments' ../../../data/Processed/Model_Creation/1-After_KD_Selection/KD_dataset.csv  
```

The dataset at this point of time contained a total of 83083 rows and 32 columns.

## Data Processing ##

Data processing is essential whenever raw data is given. This step aims to refine raw data into information that can be used in the analysis or as training data for machine learning models.

First, only a few columns were selected for further analysis after a thorough study of the raw dataset: compound_id, standard_inchi_key, target_id, standard_type, standard_value and standard_units. The columns compound_id and standard_inchi_key were selected because their information makes it possible to add the additional columns standard_inchi and canonical_smiles, which information will be used to add compound features. The target_id column contains uniprot IDs that will be used to retrieve and then insert the corresponding protein sequences into a column. Thereafter, protein features based on sequences will be added. The most important column is standard_value which contains the KD values. The remaining two columns, standard_type and standard_units, allow the conversion of the KD values to the right units.

The next step of processing the dataset was removing NaNs. For the columns compound_id and standard_inchi_key, 3305 and 2996 NaNs were found, respectively. However, only one of these columns is necessary to add the additional columns, therefore only 1756 rows with NaN in both these columns were removed. 2281, 16, 586 and 0 NaNs were found in the columns target_id, standard_value, standard_units and standard_type, respectively. These NaNs can not be filled with the information present in the dataset, as a result they were removed.

While dealing with the NaNs, inconsistencies and problems in the dataset were detected. One of the problems in the dataset were the multiple kinase IDs in the column target_id. This is due to the fact that the assay did not specify which kinase was used so the IDs of all possible kinases were enlisted. If the correct drug-kinase interaction is unknown, all these rows must be deleted. In total 3169 rows were removed from the dataset. An inconsistency in the data appeared in the columns standard_value and standard_units. The values in the column standard_value had different units as it is possible to see in the table below.

| Units       | Occurrences |
|-------------|-------------|
| NM          | 74364       |
| -LOG(10) M  | 668         |
| 10'-1/S     | 49          |
| 10'-2/S     | 40          |
| 10'-3/S     | 33          |
| MIN-1       | 22          |
| M           | 20          |
| NMOL/L      | 19          |
| /S          | 16          |
| S-1         | 13          |
| MM          | 13          |
| /UM         | 8           |
| UG ML-1     | 6           |
| 10'-4/S     | 5           |
| UL/OD/MIN   | 3           |
| NO_UNIT     | 2           |
| KCAL/MOL    | 2           |
| 10'-9L/MOL  | 1           |
| 10'3/S      | 1           |
| 10'-6/S     | 1           |
| 1/S         | 1           |
| 10'-10L/MOL | 1           |
| 10'1/S      | 1           |
| 10'-5/S     | 1           |

To have a consistence dataset the values have to be in the same unit, Molar. Thus, the values in standard_value column were converted to Molar. It was only possible to convert the MM, -LOG(10) M, NM and NMOL/L units, since the other units needed additional information to convert them but unfortunately the information was not available and so 226 rows were removed. Then, the values were converted to pKD using -log10 and, as such, the values before the conversion could not be equal to 0, thus rows containing a 0 were eliminated. Only 1 row was eliminated. Another inconsistency arouse, but this time in the column standard_type. It contains several KD types as it is possible to see below. 

| Type     | occurrences |
|----------|-------------|
| KD       | 67304       |
| KDAPP    | 5137        |
| Kd       | 1798        |
| PKD      | 668         |
| KD/KI    | 61          |
| KD1      | 33          |
| KD2      | 31          |
| KDISS    | 15          |
| KD APP   | 11          |
| KD HYDRO | 4           |
| KDSPR    | 1           |

To find out what these various types mean, the publications related to the values were read. Only the KD/KI type, 61 rows, was removed due to the fact that the KI value is unknown and therefore it can not be converted to pKD. The rest of the types represent the same thing, the dissociation constant. Since the columns standard_units and standard_type are not required for further processing and analysis they were removed.

To add the canonical_smiles and standard_inchi columns, a file containing these different chemical representations was downloaded from the database ChEMBL, a manually curated chemical database of bioactive molecules with drug-like properties. To download and extract the file the following UNIX commands were used:

```bash
wget http://ftp.ebi.ac.uk/pub/databases/chembl/ChEMBLdb/releases/chembl_24_1/chembl_24_chemreps.txt.gz
gunzip chembl_24_chemreps.txt.gz
```

After extracting, the information in the columns compound_id and standard_inchi_key were used to add the additional columns. Unfortunately not all values were set in the two additional columns, because 660 and 655 NaNs were found in standard_inchi and canonical_smiles, respectively. Fortunately, compound features can be added using only one of these columns, therefore only 655 rows that contained NaN in both columns were removed. The columns compound_id and standard_inchi_key were removed because they are not longer needed.

The kinases studied in this challenge are from _Homo Sapiens_ so the human proteome was downloaded from Uniprot as a tab separated file with two columns: Entry and Sequence. The column Entry was renamed as target_id. Then the dataset of the proteome was inner joined on the column target_id with the previous dataset to add the Sequence column. Since, the target_id will not be required it was removed, resulting in a merged dataset of 66618 rows and 4 columns. These number of rows can be explained by the fact that the previous dataset contained non-human kinases. 

## Feature Selection ##

In the TODO list

### Protein Feature Selection ###

The isoeletric point, molecular weight, aromaticity, instability index and gravy of the kinases were added to the dataset using the biopython package, which requires the protein sequences in the column Sequence. Using the package pydpi the amino-acid composition was also added.

These features were selected on kinases as a way of predicting these proteins behaviour and characteristics that are important in drug-kinase bonding interaction. Such characteristics are mainly refered to the chemical properties of the proteins.

### Drug Feature Selection ###

in the TODO list

## Machine Learning Modelling ##

In this step, all the data gathered until now is going to be used to make machine learning models capable of predicting KD values of drug-kinase interactions.

KD values of the training data are known so supervised learning algorithms are going to be use, specially regression algorithms, since the KD values are continuous numerical values. The algorithms selected to be tested in a scikit-learn pipeline are the support vector regression (SVR), with both linear and rbf kernels, and ridge. This selection as based on the flow chart given by scikit-learn.

First, the column standard_value was set to a variable called y, which represents the target. Then, the other columns were set to the variable X except for the columns: standard_value, standard_inchi, canonical_smiles and Sequence. The variable X represent the features. Then, two scikit-learn pipelines were created with a scaler step, which uses the function `StandardScaler()` and a classifier step, one with SVR and the other with the Ridge algorithm. The SVR  was tested using different values for the Kernel (linear and rbf), C (1 , 10 and 100) and Gamma (0.001 and 0.0001). In Ridge, only different Alpha values were tested (0.01 and 100). To test the different parameters, GridSearchCV was used with a value of cross-validation of 3. The best algorithm and parameters were selected and dumped using python's pickle module automatically.

## 1º Iteration Prediction ##

### Results ###

To evaluate the regression ML models several error metrics were used: r2 or coefficient of determination, explained variance score, mean absolute error, mean squared error and median absolute error.
The R2 score is a regression metric used to assess the goodness of fit of the regression model. It ranges between 0 and 1 indicating poor fit and perfect fit respectively. 

The mean absolute error is the average of the difference between the real values and the predictive values. The mean squared error is the average of the square of the difference between real and predictive values.
Both mean absolute error and mean squared error are somewhat sensitive to outliers, therefore median absolute error was also used, which is, basically, insensitive to outliers giving a more robust measure of the model performance.



For SVR:

SVR_r2_score - 0.24574847521905796

SVR_explained_variance_score - 0.28704651951763405

SVR_mean_absolute_error - 0.6676763246531715

SVR_mean_squared_error - 1.1857157488586791

SVR_median_absolute_error - 0.24600883476015722

For Ridge:

Ridge_r2_score - 0.18506675477005663

Ridge_explained_variance_score - 0.1851839615594476

Ridge_mean_absolute_error - 0.8467971924425742

Ridge_mean_squared_error - 1.2811100162088411

Ridge_median_absolute_error - 0.6392311521644425



