#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# IDG_Dream_Project
# Copyright (C) 2018  IDG_Dream

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import pandas as pd 
import pickle
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score, explained_variance_score, mean_absolute_error, mean_squared_error, median_absolute_error
from sklearn.svm import SVR
from sklearn.linear_model import Ridge 

# Read CSV

ds = pd.read_csv('../../../data/Processed/Model_Creation/3-After_Feature_Selection/2-ds_pydpi.csv')

# Select features to use in ML and the what to predict

y = ds['standard_value']
x = ds.drop(['standard_value', 'standard_inchi', 'canonical_smiles', 'Sequence'], axis=1)

# Delete ds

del ds

# Split

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.2, random_state = 33)

# Make pipe and grid for SVR

pipe_svr = Pipeline([('scaler', StandardScaler()), ('clf', SVR())])
cv_grid_svr = GridSearchCV(pipe_svr, param_grid = {'clf__C':[1,10,100], 'clf__kernel': ['rbf'], 'clf__gamma': [0.001, 0.0001]}, scoring = "r2") 
cv_grid_svr.fit(x_train,y_train)

# Make pipe and grid for Ridge

pipe_ridge = Pipeline([('scaler', StandardScaler()), ('clf', Ridge())])
cv_grid_ridge = GridSearchCV(pipe_ridge, param_grid = {'clf__alpha':[0.1,1,10]}, scoring = "r2")
cv_grid_ridge.fit(x_train,y_train)

# Print scores for SVR

y_pred = cv_grid_svr.predict(x_test)

print("SVR_r2_score - {}".format(r2_score(y_test,y_pred)))
print("SVR_explained_variance_score - {}".format(explained_variance_score(y_test,y_pred)))
print("SVR_mean_absolute_error - {}".format(mean_absolute_error(y_test,y_pred)))
print("SVR_mean_squared_error - {}".format(mean_squared_error(y_test,y_pred)))
print("SVR_median_absolute_error - {}".format(median_absolute_error(y_test,y_pred)))

# Print scores for ridge

y_pred = cv_grid_ridge.predict(x_test)

print("Ridge_r2_score - {}".format(r2_score(y_test,y_pred)))
print("Ridge_explained_variance_score - {}".format(explained_variance_score(y_test,y_pred)))
print("Ridge_mean_absolute_error - {}".format(mean_absolute_error(y_test,y_pred)))
print("Ridge_mean_squared_error - {}".format(mean_squared_error(y_test,y_pred)))
print("Ridge_median_absolute_error - {}".format(median_absolute_error(y_test,y_pred)))

# Dump best model and params

if cv_grid_svr.best_score_ > cv_grid_ridge.best_score_:
    with open('../../../data/Processed/Model_Creation/4-After_Model_Persistence/Model.pickle', 'wb') as f:
        pickle.dump(cv_grid_svr,f)
else:
    with open('../../../data/Processed/Model_Creation/4-After_Model_Persistence/Model.pickle', 'wb') as f:
        pickle.dump(cv_grid_ridge,f)

