# -*- coding: utf-8 -*-

# IDG_Dream_Project
# Copyright (C) 2018  IDG_Dream

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import pandas as pd
import numpy as np
from pydpi.pypro import AAComposition
from pydpi.pypro import CTD

# Load dataset

ds = pd.read_csv('../../../data/Processed/Model_Creation/3-After_Feature_Selection/1-ds_biopython.csv')

# Set aminoacids into a list

aa = ['A','R','N','D','C','E','Q','G','H','I','L','K','M','F','P','S','T','W','Y','V']

# Assign columns of the AA composition

for i in aa:
    ds[i] = np.NaN

del aa 

# Add AA composition features

uniq_seqs = ds['Sequence'].drop_duplicates().values 

for seq in uniq_seqs:
    aacomp = AAComposition.CalculateAAComposition(seq)
    for i in aacomp:
        ds.loc[ds['Sequence'] == seq, i] = aacomp[i]

# Rename

ds.rename(columns={'A':'comp_A', 'C':'comp_C', 'D':'comp_D', 'E':'comp_E', 'F':'comp_F', 'G':'comp_G', 'H':'comp_H', 'I':'comp_I', 'K':'comp_K', 'L':'comp_L', 'M':'comp_M', 'N':'comp_N', 'P':'comp_P', 'Q':'comp_Q', 'R':'comp_R', 'S':'comp_S', 'T':'comp_T', 'V':'comp_V', 'W':'comp_W', 'Y':'comp_Y' }, inplace=True)

# Set CTD 

ctd = ['_NormalizedVDWVD1075', '_PolarityD1075', '_SecondaryStrD3025', '_PolarityD3100', '_ChargeD1100', '_SecondaryStrT23', '_PolarityD3025', '_NormalizedVDWVC1', '_NormalizedVDWVC3', '_HydrophobicityT23', '_SolventAccessibilityD1025', '_PolarityD1100', '_NormalizedVDWVD2100', '_ChargeD3050', '_PolarityD2001', '_SolventAccessibilityD2025', '_HydrophobicityD3050', '_PolarityT12', '_PolarityT13', '_ChargeT23', '_HydrophobicityD2100', '_SolventAccessibilityD3001', '_ChargeD1075', '_SecondaryStrD1075', '_PolarizabilityD2050', '_SolventAccessibilityD3100', '_PolarizabilityD1075', '_HydrophobicityD3025', '_PolarizabilityD3025', '_SecondaryStrD3050', '_SolventAccessibilityD2050', '_HydrophobicityD2001', '_SecondaryStrD2075', '_PolarityD3050', '_PolarityD1001', '_ChargeD2075', '_NormalizedVDWVD3025', '_SolventAccessibilityD1100', '_SecondaryStrD1100', '_NormalizedVDWVD1001', '_NormalizedVDWVD2050', '_NormalizedVDWVC2', '_PolarizabilityC2', '_PolarizabilityC3', '_NormalizedVDWVD3001', '_PolarizabilityC1', '_SecondaryStrC2', '_SecondaryStrC3', '_ChargeT12', '_ChargeT13', '_ChargeD1001', '_ChargeD1025', '_NormalizedVDWVD2025', '_NormalizedVDWVD3050', '_SecondaryStrD1025', '_SolventAccessibilityD1050', '_PolarizabilityD3001', '_PolarityD2075', '_SecondaryStrD3001', '_ChargeD2025', '_HydrophobicityD2050', '_PolarizabilityD1100', '_HydrophobicityC1', '_HydrophobicityC2', '_SolventAccessibilityD2100', '_PolarizabilityD2001', '_PolarizabilityD1025', '_NormalizedVDWVT13', '_PolarizabilityD3050', '_SolventAccessibilityT13', '_SolventAccessibilityT12', '_PolarityD1050', '_ChargeD3075', '_SolventAccessibilityD1001', '_HydrophobicityC3', '_HydrophobicityD3100', '_SecondaryStrD1050', '_ChargeD3001', '_SecondaryStrD2050', '_PolarityD2025', '_NormalizedVDWVD2001', '_HydrophobicityD3075', '_SecondaryStrD2025', '_SolventAccessibilityD2001', '_HydrophobicityD1025', '_ChargeD1050', '_SolventAccessibilityD3025', '_PolarizabilityD3075', '_PolarityD2100', '_SecondaryStrD3100', '_PolarizabilityT13', '_PolarizabilityT12', '_SecondaryStrD2001', '_NormalizedVDWVD1050', '_PolarizabilityD1050', '_SolventAccessibilityT23', '_ChargeC3', '_ChargeC2', '_ChargeC1', '_NormalizedVDWVT23', '_NormalizedVDWVD3100', '_PolarizabilityD2025', '_SolventAccessibilityD3075', '_HydrophobicityD2075', '_HydrophobicityD1075', '_ChargeD2050', '_ChargeD3100', '_SecondaryStrC1', '_ChargeD3025', '_PolarityD3001', '_PolarityD3075', '_PolarityD1025', '_SecondaryStrT13', '_SecondaryStrT12', '_HydrophobicityD1001', '_SolventAccessibilityD1075', '_HydrophobicityD1050', '_HydrophobicityT13', '_HydrophobicityT12', '_SecondaryStrD1001', '_NormalizedVDWVD1100', '_NormalizedVDWVD3075', '_NormalizedVDWVD1025', '_SolventAccessibilityD2075', '_SecondaryStrD2100', '_PolarizabilityD3100', '_SecondaryStrD3075', '_NormalizedVDWVT12', '_PolarizabilityT23', '_SolventAccessibilityD3050', '_PolarityT23', '_PolarityD2050', '_HydrophobicityD2025', '_PolarizabilityD2075', '_HydrophobicityD3001', '_HydrophobicityD1100', '_ChargeD2001', '_SolventAccessibilityC1', '_SolventAccessibilityC2', '_SolventAccessibilityC3', '_PolarizabilityD1001','_NormalizedVDWVD2075', '_ChargeD2100', '_PolarizabilityD2100', '_PolarityC1', '_PolarityC3', '_PolarityC2']

# Assign columns for CTD

for i in ctd:
    ds[i] = np.NaN

del ctd

# Assign columns for CTD

for seq in uniq_seqs:
    ctd_feats = CTD.CalculateCTD(seq)
    for i in ctd_feats:
        ds.loc[ds['Sequence'] == seq, i] = ctd_feats[i]

del uniq_seqs

# Save dataset

ds.to_csv('../../../data/Processed/Model_Creation/3-After_Feature_Selection/2-ds_pydpi.csv', index=False)
