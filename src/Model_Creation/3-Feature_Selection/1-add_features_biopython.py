#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# IDG_Dream_Project
# Copyright (C) 2018  IDG_Dream

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import pandas as pd
import numpy as np
from Bio.SeqUtils.ProtParam import ProteinAnalysis

# Load dataset

ds = pd.read_csv('../../../data/Processed/Model_Creation/2-After_Data_Preparation/ds_prep.csv')

# Assign new columns for the features

ds = ds.assign(ip = np.NaN)
ds = ds.assign(mw = np.NaN)
ds = ds.assign(arom = np.NaN)
ds = ds.assign(ii = np.NaN)
ds = ds.assign(gravy = np.NaN)

# Apply func to calculate the various features

seq_array = ds['Sequence'].drop_duplicates().values

for seq in seq_array:
    ds.loc[ds['Sequence'] == seq, 'ip'] = ProteinAnalysis(seq).isoelectric_point()
    ds.loc[ds['Sequence'] == seq, 'mw'] = ProteinAnalysis(seq).molecular_weight()
    ds.loc[ds['Sequence'] == seq, 'arom'] = ProteinAnalysis(seq).aromaticity()
    ds.loc[ds['Sequence'] == seq, 'ii'] = ProteinAnalysis(seq).instability_index()
    ds.loc[ds['Sequence'] == seq, 'gravy'] = ProteinAnalysis(seq).gravy()

# Save dataset

ds.to_csv('../../../data/Processed/Model_Creation/3-After_Feature_Selection/1-ds_biopython.csv', index=False)



