#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# IDG_Dream_Project
# Copyright (C) 2018  IDG_Dream

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import pandas as pd
import numpy as np

# Load round 1 csv

ds = pd.read_csv('../../../../data/Preprocessed/Round_1/round_1_template.csv')

# Load human proteome dataset

ds_prot = pd.read_csv('../../../../data/Preprocessed/Uniprot/human_proteome.tab', sep='\t')

# Rename column of ds_prot

ds_prot.rename(columns={'Entry':'UniProt_Id'}, inplace=True)

# Merge the to dataset

merged_ds = pd.merge(ds, ds_prot, how='inner', on=['UniProt_Id'])

# Keep necessary column

trim_ds = merged_ds[["Sequence"]]

# Save dataset

trim_ds.to_csv('../../../../data/Processed/KD_Prediction/Round_1/1-After_Data_Preparation/ds_prep.csv', index=False)

