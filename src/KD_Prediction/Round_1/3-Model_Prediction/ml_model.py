#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# IDG_Dream_Project
# Copyright (C) 2018  IDG_Dream

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import pandas as pd 
import pickle
from sklearn.pipeline import Pipeline

# Read CSV

ds = pd.read_csv('../../../../data/Processed/KD_Prediction/Round_1/2-After_Feature_Selection/2-ds_pydpi.csv')

# Select features to use in ML and the what to predict

ds_pred = ds.drop(['Sequence'], axis=1)

# Load model and params

with open('../../../../data/Processed/Model_Creation/4-After_Model_Persistence/Model.pickle', 'rb') as f:
    model = pickle.load(f)

# Predict Round_1

kd_value_pred = model.predict(ds_pred)

# Read original csv

ds_round_1 = pd.read_csv("../../../../data/Preprocessed/Round_1/round_1_template.csv")

# Insert kd_values into ds_round_1

ds_round_1["pKd_[M]_pred"] = kd_value_pred

# Save csv

ds_round_1.to_csv('../../../../data/Processed/KD_Prediction/Round_1/3-After_Model_Prediction/round_1_pred.csv', index=False)






